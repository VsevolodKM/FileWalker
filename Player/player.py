import random
import os
import sys
from PyQt5.QtGui import QImage, QColor, QPen
from PyQt5.QtCore import Qt

def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


class Player:
    Greetings = [
        [
            'Hi',
            'Hello!',
            'Hello there!'
        ],
        [
            'Привет!',
            'Здорова!',
            'Здравствуйте!'
        ]

    ]

    FileSuccess = [
        [
            'I have opened a file!',
            'Yeah, file!',
            'It was easy!'
        ],
        [
            'Я открыл файл!',
            'Круто, файл!',
            'Это было легко!'
        ]
    ]

    NewDir = [
        [
            'I have created a dir!',
            'Yeah, new directory!',
            'It was easy!'
        ],
        [
            'Я создал новую папку!',
            'Круто, новая папка!',
            'Это было легко!'
        ]
    ]

    NoFile = [
        [
            'I have no files to open',
            'Open what?'
        ],
        [
            'У меня нет файлов',
            'Что открыть?'
        ]
    ]

    NoAccess = [
        [
            'I have no permission',
            'Sorry, I cant',
            'I have no access',
            'OS said me not to open'
        ],
        [
            'У меня нет разрешения',
            'Прости, не могу',
            'У меня нет доступа',
            'ОС сказала мне не открывать'
        ]
    ]

    Delete = [
        [
            'I have deleted it',
            'I will miss',
            'Goodbye, file',
            'Goal!'
        ],
        [
            'Я удалил это',
            'Я буду скучать',
            'Прощай, файл',
            'Я попал!'
        ]
    ]

    Copy = [
        [
            'I have copied it!',
            'Now there are two files',
            'File copied'
        ],
        [
            'Я скопировал это!',
            'Теперь тут два таких файла',
            'Файл скопирован'
        ]
    ]

    Coffee = [
        [
            'Coffee time!',
            'I want coffee',
            'Coffee break',
            'I want tea'
        ],
        [
            'Время кофе!',
            'Я хочу кофе',
            'Надо отдохнуть',
            'Я хочу чая'
        ]
    ]

    What = [
        [
            'What the...',
            'I dont know what it is',
            'What is it?',
            'Very strange'
        ],
        [
            'Это что?',
            'Это вообще что?',
            'Я не знаю что это',
            'Очень странно'
        ]
    ]

    Realy = [
        [
            'Realy?',
            'Are you sure?'
        ],
        [
            'Точно?',
            'Ты уверен?'
        ]
    ]

    NewLevel = [
        [
            'New level!',
            'Yeah, new level!',
            'New level, cool!'
        ],
        [
            'Новый уровень!',
            'Да, новый уровень!',
            'Новый уровень, круто!'
        ]
    ]

    ProjectDir = [
        [
            'I was here...',
            'Looks familiar...',
            'I have strange feeling...'
        ],
        [
            'Хм, я тут уже был...',
            'Место кажется знакомым...',
            'У меня странное чувство...'
        ]
    ]

    Exists = [
        [
            'Something already has that name',
            'There are something with the same name'
        ],
        [
            'Такое имя уже есть',
            'Уже есть что-то с таким именем'
        ]
    ]

    Rename = [
        [
            'I have renamed it!',
            'Now it has a new name'
        ],
        [
            'Я переименовал это!',
            'Теперь у него другое имя'
        ]
    ]

    def __init__(self, window):
        self.x = 200
        self.y = 200
        self.l = window.language
        self.select = None
        self.idea = ''
        self.img = QImage(resource_path('Images/player_walk.png'))
        self.img_coffee1 = QImage(resource_path('Images/player_coffee1.png'))
        self.img_coffee2 = QImage(resource_path('Images/player_coffee2.png'))
        self.img_xp = QImage(resource_path('Images/xp_bar.png'))
        self.item = None
        self.xp = 0
        self.level = 1
        self.xp_in_level = 20

    def draw(self, w, painter):
        self.x = min(self.x, (w.width() + 50) // 25 * 25)
        self.x = max(self.x, -50)
        self.y = min(self.y, (w.height() + 50) // 25 * 25)
        self.y = max(self.y, -50)
        if w.coffee_time >= 1500:
            w.scroller.active = False
            already_drinking  = False
            for i in range(2):
                for j in range(len(Player.Coffee[i])):
                    if Player.Coffee[i][j] == self.idea:
                        already_drinking = True
            if not already_drinking:
                self.say('Coffee')
            if w.coffee_time % 200 < 160:
                painter.drawImage(self.x, self.y, self.img_coffee1)
            else:
                painter.drawImage(self.x, self.y, self.img_coffee2)
            if w.coffee_time >= 2150:
                self.idea = ''
                w.coffee_time = 0
        else:
            painter.drawImage(self.x, self.y, self.img)
        if self.idea != '':
            painter.setPen(QColor(255, 150, 0))
            painter.setFont(w.gui_settings.Fonts['small_font'])
            painter.drawText(self.x + 55, self.y - 10, len(self.idea) * 20, 50, Qt.AlignLeft, self.idea)
        if self.item:
            painter.drawImage(self.x + 35, self.y + 25, self.item.img)
        self.draw_xp(w, painter)

    def draw_xp(self, w, painter):
        painter.drawImage(int(w.width() / 2 - 150), 18, self.img_xp)
        painter.setPen(w.gui_settings.Colors['selected_color'])
        painter.setFont(w.gui_settings.Fonts['small_font'])
        painter.drawText(int(w.width() / 2 - 140), 24, 75, 20, Qt.AlignLeft, 'xp: ' + str(self.xp))
        painter.drawText(int(w.width() / 2 - 65), 24, 75, 20, Qt.AlignLeft, 'lvl: ' + str(self.level))
        painter.setPen(QPen(w.gui_settings.Colors['selected_color'], 3, Qt.SolidLine))
        painter.drawLine(int(w.width() / 2), 32, int(w.width() / 2 + 132 * self.xp / self.xp_in_level), 32)

    def get_xp(self, xp):
        self.xp += xp
        if self.xp >= self.xp_in_level:
            self.level += 1
            self.xp = self.xp % self.xp_in_level
            self.xp_in_level = self.level * 20
            self.say('NewLevel')

    def say(self, idea):
        if idea == 'Greeting':
            self.idea = self.Greetings[self.l][random.randint(0, 2)]
        elif idea == 'OpenedFile':
            self.idea = self.FileSuccess[self.l][random.randint(0, 2)]
        elif idea == 'NoFile':
            self.idea = self.NoFile[self.l][random.randint(0, 1)]
        elif idea == 'NoAccess':
            self.idea = self.NoAccess[self.l][random.randint(0, 3)]
        elif idea == 'Delete':
            self.idea = self.Delete[self.l][random.randint(0, 3)]
        elif idea == 'Copy':
            self.idea = self.Copy[self.l][random.randint(0, 2)]
        elif idea == 'Coffee':
            self.idea = self.Coffee[self.l][random.randint(0, 3)]
        elif idea == 'Realy':
            self.idea = self.Realy[self.l][random.randint(0, 1)]
        elif idea == 'NewLevel':
            self.idea = self.NewLevel[self.l][random.randint(0, 2)]
        elif idea == 'ProjectDir':
            self.idea = self.ProjectDir[self.l][random.randint(0, 2)]
        elif idea == 'What':
            self.idea = self.What[self.l][random.randint(0, 3)]
        elif idea == 'Exists':
            self.idea = self.Exists[self.l][random.randint(0, 1)]
        elif idea == 'Rename':
            self.idea = self.Rename[self.l][random.randint(0, 1)]
        elif idea == 'NewDir':
            self.idea = self.NewDir[self.l][random.randint(0, 2)]
        else:
            self.idea = 'I wanted to say something...'


import os
import sys

here = os.path.dirname(__file__)

sys.path.append(os.path.join(here, '..'))

from GUI_Settings.gui_settings import GUISetting

from .item import Item
from.file_system import FileSystem

class Tree:

    def __init__(self):
        self.dir = os.getcwd()
        self.objects = []
        self.tree_list = []
        self.settings = GUISetting()
        self.ultraviolet_on = False
        self.level = 25

    def is_hidden(self, item: str):
        return item.startswith('.')

    def remove_hidden(self):
        l = len(self.tree_list)
        i = 0
        while i < l:
            if self.is_hidden(self.tree_list[i]):
                self.tree_list.remove(self.tree_list[i])
                i -= 1
                l -= 1
            i += 1

    def update(self):
        self.level = 25
        self.tree_list = FileSystem.get_tree()
        if not self.ultraviolet_on:
            self.remove_hidden()
        self.objects = [Item(self.tree_list[i], self.tree_list[i], os.path.isfile(self.tree_list[i]))
                        for i in range(len(self.tree_list))]

    def go_home(self):
        FileSystem.go_home()
        self.dir = os.getcwd()

    def go_back(self):
        FileSystem.go_back()
        self.dir = os.getcwd()

    def change_dir(self, player):
        if player.select:
            FileSystem.change_dir(tree=self, player=player)
            player.select = None

    def start_file(self, player):
        if player.item:
            FileSystem.start_file(tree=self, player=player)
        else:
            player.say('NoFile')

    def remove_item(self, player):
        if player.item:
            FileSystem.remove_item(player=player)
            self.update()
            print('Delete: ', player.item.path)
            player.item = None
            player.say('Delete')
            player.get_xp(10)
        else:
            player.say('NoFile')

    def copy_item(self, player):
        if player.item:
            FileSystem.copy_item(tree=self, player=player)
        else:
            player.say('NoFile')

    def rename_item(self, player, new_name):
        if player.select.type >= 2:
            player.say('What')
        else:
            if os.path.exists(new_name):
                player.say('Exists')
            else:
                FileSystem.rename_item(tree=self, player=player, new_name=new_name)

    def create_dir(self, player, dir_name):
        if os.path.exists(dir_name):
            player.say('Exists')
        else:
            FileSystem.create_dir(tree=self, player=player, dir_name=dir_name)

    def show_tree(self, w, painter):
        for i in range(len(self.tree_list)):
            self.objects[i].y = self.level + 25 * i
            self.objects[i].check_player(w)
            self.objects[i].draw(w, painter)

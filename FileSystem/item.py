import os
from PyQt5.QtCore import Qt

from GUI_Settings.gui_settings import GUISetting

class Item:
    def __init__(self, name="DefaultItem", path = '/', type = 0):
        self.name = name
        self.path = path
        if os.path.isfile(path):
            self.type = 0
        elif os.path.isdir(path):
            self.type = 1
        else:
            self.type = 2
        self.x = 50
        self.y = 0
        self.settings = GUISetting()
        if self.path.startswith('.'):
            self.color = self.settings.Colors['white_color']
        else:
            self.color = self.settings.Colors['object_color']
        self.img = None

    def draw(self, w, painter):
        painter.setPen(self.color)
        painter.setFont(w.gui_settings.Fonts['basic_font'])
        painter.drawText(int(self.x + 25), int(self.y), min(len(self.name) * 20, 500), 50, Qt.AlignLeft, self.name)
        if self.type == 0:
            self.img = w.file_icon
        elif self.type == 1:
            self.img = w.dir_icon
        else:
            self.img = w.unknown_icon
        painter.drawImage(int(self.x), int(self.y), self.img)

    def check_player(self, w):
        if w.player.y + 25 == self.y and w.player.x - self.x < max(len(self.name) * 14 + 25, 75) and w.player.x > self.x:
            w.player.select = self
            self.color = self.settings.Colors['selected_color']
        else:
            if self.name.startswith('.'):
                self.color = self.settings.Colors['white_color']
            else:
                self.color = self.settings.Colors['object_color']
            if w.player.select:
                if w.player.select.path == self.path:
                    w.player.select = None


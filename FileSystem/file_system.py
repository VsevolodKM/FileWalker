
import os
import shutil
import sys

here = os.path.dirname(__file__)

sys.path.append(os.path.join(here, '..'))


class FileSystem:
    def go_home():
        try:
            os.chdir(os.path.expanduser('~'))
        except OSError:
            print('OS error')
    
    def go_back():
        try:
            os.chdir('..')
        except OSError:
            print('OS error')
        print('Back')

    def get_tree():
        l = ['This directory is hidden by system']
        try:
            l = os.listdir()
        except OSError:
            print('OS error')
        return l
    
    def remove_item(player):
        if os.path.isfile(player.item.path):
            if os.access(player.item.path, os.R_OK):
                try:
                    os.remove(player.item.path)
                except OSError:
                    print('OS error')
                    player.idea = 'Error!'
        elif os.path.isdir(player.item.path):
            try:
                shutil.rmtree(player.item.path)
            except OSError:
                print('OS error')
                player.idea = 'Error!'

    def change_dir(tree, player):
        if os.path.isdir(player.select.path):
            if os.access(player.select.path, os.X_OK):
                try:
                    os.chdir(player.select.path)
                except OSError:
                    print('OS error')
                    player.idea = 'Error!'
                tree.dir = os.getcwd()
                player.get_xp(1)
                print('chdir: ', player.select.path)
                tree.update()
                if player.select.path == 'FileWalker':
                    player.get_xp(10)
                    player.say('ProjectDir')
                else:
                    player.idea = ''
            else:
                player.say('NoAccess')
        else:
            if not os.path.isfile(player.select.path):
                player.say('What')
            print('Not a dir')

    def start_file(tree, player):
        if os.path.isfile(player.item.path):
            if os.access(player.item.path, os.R_OK):
                if os.name == 'posix':
                    try:
                        os.system("open " + '"' + player.item.path + '"')
                    except OSError:
                        print('OS error')
                        player.idea = 'Error!'
                elif os.name == 'nt':
                    try:
                        os.startfile(player.item.path)
                    except OSError:
                        print('OS error')
                        player.idea = 'Error!'
                else:
                    try:
                        os.system(player.item.path)
                    except OSError:
                        print('OS error')
                        player.idea = 'Error!'
                tree.dir = os.getcwd()
                print('open file: ', player.item.path)
                player.say('OpenedFile')
                player.get_xp(5)
            else:
                player.say('NoAccess')
        else:
            if not os.path.isdir(player.item.path):
                player.say('What')
            print('Not a file')

    def copy_item(tree, player):
        player.idea = "..."
        new_name = player.item.name
        i = 1
        if os.path.exists(new_name):
            while os.path.exists('copy' + str(i) + '_' + new_name):
                print('Already exists')
                i += 1
            new_name = 'copy' + str(i) + '_' + new_name
        if os.path.isfile(player.item.path):
            try:
                shutil.copy(player.item.path, os.path.join(tree.dir, new_name))
                print('Copy: ', player.item.path, ' to ', tree.dir)
                player.say('Copy')
                player.get_xp(10)
            except OSError:
                print('OS error')
                player.idea = 'Error!'
        elif os.path.isdir(player.item.path):
            try:
                shutil.copytree(player.item.path, os.path.join(tree.dir, new_name))
                print('Copy: ', player.item.path, ' to ', tree.dir)
                player.say('Copy')
                player.get_xp(10)
            except OSError:
                print('OS error')
                player.idea = 'Error!'
        else:
            player.say('What')
        tree.update()

    def rename_item(tree, player, new_name):
        try:
            os.rename(player.select.name, new_name)
            print('Rename: ', player.select.name, ' to ', new_name)
            player.say('Rename')
            player.get_xp(10)
            tree.update()
        except OSError:
            print('OS error')
            player.idea = 'Error!'

    def create_dir(tree, player, dir_name):
        try:
            os.mkdir(dir_name)
            print('New dir: ', dir_name)
            player.say('NewDir')
            player.get_xp(10)
            tree.update()
        except OSError:
            print('OS error')
            player.idea = 'Error!'


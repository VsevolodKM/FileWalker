from PyQt5.QtGui import QColor, QFont


class GUISetting:
    Colors = {
        'background_color':  QColor(0, 40, 50),
        'ultraviolet_color': QColor(25, 0, 60),
        'white_color':       QColor(250, 200, 250),
        'object_color':      QColor(0, 180, 220),
        'selected_color':    QColor(255, 150, 0),
        'terminal_color': QColor(0, 20, 10),
        'error_color': QColor(250, 10, 0)
    }

    Fonts = {
        'basic_font': QFont('Tahoma', 15),
        'small_font': QFont('Tahoma', 10)
    }

    def __init__(self):
        pass

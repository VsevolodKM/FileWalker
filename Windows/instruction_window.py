from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtGui import QColor, QPainter, QBrush, QPen
from PyQt5.QtCore import Qt

class InstructionWindow(QMainWindow):

    def __init__(self, w):
        super().__init__(w)
        self.setWindowTitle('Instruction')
        self.w = w
        self.l = w.language
        self.painter = QPainter(self)
        self.gui_settings = w.gui_settings
        self.setAttribute(Qt.WA_NoSystemBackground, True)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setGeometry(100, 10, 800, 100)

    def paintEvent(self, event):
        super().paintEvent(event)
        self.painter.begin(self)
        self.painter.setOpacity(0.6)
        self.painter.setBrush(QBrush(QColor(0, 50, 50)))
        self.painter.setPen(QPen(Qt.transparent))
        self.painter.drawRect(self.rect())

        self.painter.setOpacity(1.0)

        self.painter.setFont(self.w.gui_settings.Fonts['basic_font'])

        self.painter.setPen(self.gui_settings.Colors['object_color'])
        if self.l == 0:
            self.painter.drawText(0, 0, 800, 50, Qt.AlignLeft, 'Control: ')
            self.painter.drawText(25, 25, 800, 50, Qt.AlignLeft, 'Movement - arrows or WASD')
            self.painter.drawText(25, 50, 800, 50, Qt.AlignLeft, 'Choose file or directory - Q')
            self.painter.drawText(25, 75, 800, 50, Qt.AlignLeft, 'Enter in directory / interaction with things - E')
            self.painter.drawText(25, 100, 800, 50, Qt.AlignLeft, 'Rename file or directory - R')
            self.painter.drawText(25, 125, 800, 50, Qt.AlignLeft, 'Create new directory - C')
        else:
            self.painter.drawText(0, 0, 800, 50, Qt.AlignLeft, 'Управление: ')
            self.painter.drawText(25, 25, 800, 50, Qt.AlignLeft, 'Движение - стрелочки или WASD')
            self.painter.drawText(25, 50, 800, 50, Qt.AlignLeft, 'Взять файл или папку - Q')
            self.painter.drawText(25, 75, 800, 50, Qt.AlignLeft, 'Войти в папку / взаимодействие с предметами - E')
            self.painter.drawText(25, 100, 800, 50, Qt.AlignLeft, 'Переименовать файл или папку - R')
            self.painter.drawText(25, 125, 800, 50, Qt.AlignLeft, 'Создать новую папку - C')

        self.update()
        self.painter.end()

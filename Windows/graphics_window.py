from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtGui import QColor, QPainter, QBrush, QPen
from PyQt5.QtCore import Qt

class GraphicsWindow(QMainWindow):
    def __init__(self, w):
        super().__init__(w)
        self.setWindowTitle('Graphics settings')
        self.w = w
        self.painter = QPainter(self)
        self.gui_settings = w.gui_settings
        self.setAttribute(Qt.WA_NoSystemBackground, True)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setGeometry(100, 10, 300, 100)

    def paintEvent(self, event):
        super().paintEvent(event)
        self.painter.begin(self)
        self.painter.setOpacity(0.6)
        self.painter.setBrush(QBrush(QColor(0, 50, 50)))
        self.painter.setPen(QPen(Qt.transparent))
        self.painter.drawRect(self.rect())

        self.painter.setOpacity(1.0)
        self.painter.setFont(self.w.gui_settings.Fonts['basic_font'])
        self.painter.setPen(self.gui_settings.Colors['object_color'])
        if self.w.language == 0:
            self.painter.drawText(25, 25, 800, 50, Qt.AlignLeft, 'Number of particles on the background: ◀︎ ' +
                                  str(self.w.wind.particles_number) + ' ▶︎')
        else:
            self.painter.drawText(25, 25, 800, 50, Qt.AlignLeft, 'Количество частиц на заднем плане: ◀︎ ' +
                                  str(self.w.wind.particles_number) + ' ▶︎')

        self.update()
        self.painter.end()

    def keyPressEvent(self, event):
        key = event.key()

        if key == Qt.Key_A or key == Qt.Key_Left:
            if self.w.wind.particles_number > 0:
                self.w.wind.particles_number -= 1
        elif key == Qt.Key_D or key == Qt.Key_Right:
            if self.w.wind.particles_number < self.w.wind.max_number:
                self.w.wind.particles_number += 1
        else:
            super().keyPressEvent(event)

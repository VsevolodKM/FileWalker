from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtGui import QColor, QPainter, QBrush, QPen
from PyQt5.QtCore import Qt

class AboutWindow(QMainWindow):

    def __init__(self, w):
        super().__init__(w)
        self.setWindowTitle('About FileWalker')
        self.w = w
        self.l = w.language
        self.painter = QPainter(self)
        self.gui_settings = w.gui_settings
        self.setAttribute(Qt.WA_NoSystemBackground, True)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setGeometry(100, 10, 800, 100)

    def paintEvent(self, event):
        super().paintEvent(event)
        self.painter.begin(self)
        self.painter.setOpacity(0.6)
        self.painter.setBrush(QBrush(QColor(0, 50, 50)))
        self.painter.setPen(QPen(Qt.transparent))
        self.painter.drawRect(self.rect())

        self.painter.setOpacity(1.0)

        self.painter.setFont(self.w.gui_settings.Fonts['basic_font'])

        self.painter.setPen(self.gui_settings.Colors['object_color'])
        self.painter.drawText(25, 25, 800, 50, Qt.AlignLeft, 'FileWalker 1.1.0')
        if self.l == 0:
            self.painter.drawText(25, 50, 800, 50, Qt.AlignLeft, 'author: Melnik Vsevolod, 2018')
        else:
            self.painter.drawText(25, 50, 800, 50, Qt.AlignLeft, 'автор: Мельник Всеволод, 2018')

        self.update()
        self.painter.end()

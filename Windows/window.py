import os
import sys
here = os.path.dirname(__file__)

sys.path.append(os.path.join(here, '..'))

from PyQt5.QtWidgets import QMainWindow, QDesktopWidget, QAction, QLineEdit, QGraphicsDropShadowEffect
from PyQt5.QtGui import QColor, QPainter, QIcon, QImage, QBrush, QPen, QFont
from PyQt5.QtCore import Qt, QBasicTimer

from FileSystem.tree import Tree
from Player.player import Player
from GUI_Settings.gui_settings import GUISetting
from Devices.bin import Bin
from Devices.table import Table
from Devices.lamp import Lamp
from Devices.scroller import Scroller
from Devices.printer import Printer
from Wind.wind import Wind
from .line_editor import LineEditor
from .language_window import LanguageWindow
from .about_window import AboutWindow
from .graphics_window import GraphicsWindow
from .instruction_window import InstructionWindow
from TerminalWindow.terminal_window import TerminalWindow

def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)

class Window(QMainWindow):

    def __init__(self):
        super().__init__()
        self.gui_settings = GUISetting()
        self.language = 1
        self.coffee_time = 0
        self.tree = Tree()
        self.file_icon = QImage(resource_path('Images/file_icon.png'))
        self.dir_icon = QImage(resource_path('Images/dir_icon.png'))
        self.unknown_icon = QImage(resource_path('Images/unknown_icon.png'))

        self.wind = Wind(self)
        self.player = Player(self)

        self.terminal_window = TerminalWindow(self)
        self.language_window = LanguageWindow(self)
        self.graphics_window = GraphicsWindow(self)
        self.instruction = InstructionWindow(self)
        self.about_window = AboutWindow(self)
        self.line_edit = LineEditor(self)
        self.line_edit.setHidden(True)

        self.timer = QBasicTimer()
        self.timer.start(20, self)

        self.initUI()

    def initUI(self):
        self.setAttribute(Qt.WA_NoSystemBackground, True)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setGeometry(10, 10, 750, 600)
        self.center()
        self.setWindowTitle('File Walker')
        self.setWindowIcon(QIcon(resource_path('Images/player_walk.png')))

        languageAction = QAction(QIcon(resource_path('Images/player_walk.png')), ' &language', self)
        languageAction.triggered.connect(self.language_window_show)

        graphicsAction = QAction(QIcon(resource_path('Images/lamp_off.png')), ' &graphics', self)
        graphicsAction.triggered.connect(self.graphics_window_show)

        aboutAction = QAction(QIcon(resource_path('Images/file_icon.png')), ' &about', self)
        aboutAction.triggered.connect(self.about_window_show)

        instructionAction = QAction(QIcon(resource_path('Images/file_icon.png')), ' &instruction', self)
        instructionAction.triggered.connect(self.instruction_window_show)

        terminalAction = QAction(QIcon(resource_path('Images/printer.png')), ' &terminal', self)
        terminalAction.triggered.connect(self.terminal_window_show)

        menubar = self.menuBar()
        settingsMenu = menubar.addMenu(' &Settings')
        settingsMenu.addAction(languageAction)
        settingsMenu.addAction(graphicsAction)
        settingsMenu.addAction(terminalAction)
        helpMenu = menubar.addMenu(' &Help')
        helpMenu.addAction(instructionAction)
        helpMenu.addAction(aboutAction)

        self.tree.update()
        self.table = Table(300, 25)
        self.printer = Printer(300, 275)
        self.bin = Bin(300, 275)
        self.lamp = Lamp(300, 150)
        self.scroller = Scroller()
        self.player.say('Greeting')
        self.painter = QPainter(self)
        self.tree.go_home()
        self.tree.update()

        self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def language_window_show(self):
        self.language_window.setGeometry(self.x() + self.player.x, self.y() + self.player.y, 300, 100)
        self.language_window.show()

    def instruction_window_show(self):
        self.instruction.setGeometry(self.x() + self.player.x, self.y() + self.player.y, 800, 500)
        self.instruction.show()

    def graphics_window_show(self):
        self.graphics_window.setGeometry(self.x() + self.player.x, self.y() + self.player.y, 500, 70)
        self.graphics_window.show()

    def about_window_show(self):
        self.about_window.setGeometry(self.x() + self.player.x, self.y() + self.player.y, 500, 100)
        self.about_window.show()

    def terminal_window_show(self):
        self.terminal_window.setGeometry(self.x(), self.y(), 600, 400)
        self.terminal_window.show()
        print('open terminal')

    def change_language(self, l):
        self.language = l
        self.player.l = l
        self.instruction.l = l
        self.about_window.l = l

    def paintEvent(self, event):
        super().paintEvent(event)
        self.painter.begin(self)
        self.painter.setOpacity(0.938)

        if self.tree.ultraviolet_on:
            self.painter.setBrush(QBrush(self.gui_settings.Colors['ultraviolet_color']))
        else:
            self.painter.setBrush(QBrush(self.gui_settings.Colors['background_color']))
        self.painter.setPen(QPen(Qt.transparent))
        self.painter.drawRect(self.rect())

        self.wind.draw(self, self.painter)
        self.painter.setOpacity(1.0)

        self.tree.show_tree(self, self.painter)
        self.table.draw(self, self.painter)
        self.printer.draw(self, self.painter)
        self.bin.draw(self, self.painter)
        self.lamp.draw(self, self.painter)
        self.scroller.draw(self, self.painter)
        self.coffee_time += 1
        self.player.draw(self, self.painter)

        self.painter.setPen(self.gui_settings.Colors['object_color'])
        if self.tree.ultraviolet_on:
            self.painter.setBrush(QBrush(self.gui_settings.Colors['ultraviolet_color']))
        else:
            self.painter.setBrush(QBrush(self.gui_settings.Colors['background_color']))
        self.painter.drawRect(0, self.height() - 20, self.width(), 20)
        self.painter.setFont(self.gui_settings.Fonts['small_font'])
        self.painter.drawText(5, self.height() - 20, self.width(), 20, Qt.AlignLeft, self.tree.dir)

        #self.update()

        self.painter.end()

    def timerEvent(self, event):
        if event.timerId() == self.timer.timerId():
            self.update()
        else:
            super().timerEvent(event)

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_A or key == Qt.Key_Left:
            self.player.x -= 25
            if self.scroller.active:
                self.tree.level += 25
                self.scroller.rotation += 1
        elif key == Qt.Key_D or key == Qt.Key_Right:
            self.player.x += 25
            if self.scroller.active:
                self.tree.level -= 25
                self.scroller.rotation -= 1
        elif key == Qt.Key_S or key == Qt.Key_Down:
            self.player.y += 25
            if self.scroller.active:
                self.tree.level -= 25
                self.scroller.rotation -= 1
        elif key == Qt.Key_W or key == Qt.Key_Up:
            self.player.y -= 25
            if self.scroller.active:
                self.tree.level += 25
                self.scroller.rotation += 1
        elif key == Qt.Key_E:
            self.tree.change_dir(self.player)
            if abs(self.player.x + 25 - self.lamp.x - 50) < 75 and abs(self.player.y + 25 - self.lamp.y - 50) < 75:
                self.lamp.turn = not self.lamp.turn
                self.tree.ultraviolet_on = not self.tree.ultraviolet_on
                self.tree.update()

            if abs(self.player.x + 25 - self.table.x - 50) < 75 and abs(self.player.y + 25 - self.table.y - 50) < 75:
                self.tree.start_file(self.player)

            if self.bin.selected:
                f = False
                for i in range(2):
                    for j in range(len(self.player.Realy[i])):
                        if self.player.Realy[i][j] == self.player.idea:
                            f = True
                if f:
                    if self.player.item:
                        self.bin.item_img = self.player.item.img
                        self.bin.item_x = self.player.x + 35
                        self.bin.item_y = self.player.y + 30
                        self.bin.active = True
                    else:
                        self.player.say('NoFile')
                else:
                    self.player.say('Realy')

            if abs(self.player.x + 25 - self.printer.x - 50) < 75 and abs(self.player.y + 25 - self.printer.y - 50) < 75:
                if self.player.item:
                    self.printer.item = self.player.item
                    self.printer.item.x = self.printer.x
                    self.printer.item.y = self.printer.y + 50
                    self.printer.active = True
                else:
                    self.player.say('NoFile')

            if self.player.x < 40 and self.player.y > 45:
                self.scroller.active = not self.scroller.active
            else:
                self.scroller.active = False

        elif key == Qt.Key_Q:
            if self.player.select:
                self.player.item = self.player.select
                self.player.select.path = os.path.abspath(self.player.select.path)
                print('Selected: ', self.player.select.name)
                if self.player.item.type >= 2:
                    self.player.say('What')
                    self.player.get_xp(20)
            else:
                print('Nothing is selected')
        elif key == Qt.Key_R:
            if self.player.select:
                self.player.idea = ''
                self.line_edit.mode = 0
                self.line_edit.setFocus(True)
                self.line_edit.setText(self.player.select.name)
                self.line_edit.setHidden(False)
        elif key == Qt.Key_C:
            self.player.idea = ''
            self.line_edit.mode = 1
            self.line_edit.setFocus(True)
            self.line_edit.setText('')
            self.line_edit.setHidden(False)
        else:
            super().keyPressEvent(event)

        if self.player.x <= -75:
            self.tree.go_back()
            self.player.get_xp(1)
            self.tree.update()
            self.player.x = 25

        self.coffee_time = 0
        for i in range(1):
            for j in range(len(self.player.Coffee[i])):
                if self.player.Coffee[i][j] == self.player.idea:
                    self.player.idea = ''

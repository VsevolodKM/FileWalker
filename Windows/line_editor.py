from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtCore import Qt

class LineEditor(QLineEdit):

    def __init__(self, w):
        super().__init__(w)
        self.w = w
        self.mode = 0
        self.x = w.player.x + 50
        self.y = w.player.y - 20

    def setHidden(self, hidden):
        super().setHidden(hidden)
        self.x = self.w.player.x + 50
        self.y = self.w.player.y - 20
        self.setGeometry(self.x, self.y, 200, 30)
        if self.w.tree.ultraviolet_on:
            self.setStyleSheet('background-color: ' + self.w.gui_settings.Colors['ultraviolet_color'].name() +
                               '; color: ' + self.w.gui_settings.Colors['selected_color'].name() +
                               '; border: 2px solid ' + self.w.gui_settings.Colors['object_color'].name())
        else:
            self.setStyleSheet('background-color: ' + self.w.gui_settings.Colors['background_color'].name() +
                               '; color: ' + self.w.gui_settings.Colors['selected_color'].name() +
                               '; border: 2px solid ' + self.w.gui_settings.Colors['object_color'].name())

    def keyPressEvent(self, event):
        key = event.key()

        if key == Qt.Key_Return or key == Qt.Key_Enter:
            if self.mode == 0:
                self.w.tree.rename_item(self.w.player, self.text())
            else:
                self.w.tree.create_dir(self.w.player, self.text())
            self.setHidden(True)
            self.w.setFocus(True)
        else:
            super().keyPressEvent(event)

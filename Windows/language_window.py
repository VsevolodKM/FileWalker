from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtGui import QColor, QPainter, QBrush, QPen
from PyQt5.QtCore import Qt

class LanguageWindow(QMainWindow):
    def __init__(self, w):
        super().__init__(w)
        self.setWindowTitle('Choose language')
        self.choosed = 1
        self.w = w
        self.painter = QPainter(self)
        self.gui_settings = w.gui_settings
        self.setAttribute(Qt.WA_NoSystemBackground, True)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setGeometry(100, 10, 300, 100)

    def paintEvent(self, event):
        super().paintEvent(event)
        self.painter.begin(self)
        self.painter.setOpacity(0.6)
        self.painter.setBrush(QBrush(QColor(0, 50, 50)))
        self.painter.setPen(QPen(Qt.transparent))
        self.painter.drawRect(self.rect())

        self.painter.setOpacity(1.0)

        self.painter.setFont(self.w.gui_settings.Fonts['basic_font'])

        if self.choosed != 0:
            self.painter.setPen(self.gui_settings.Colors['object_color'])
        else:
            self.painter.setPen(self.gui_settings.Colors['selected_color'])
        self.painter.drawText(25, 25, 100, 50, Qt.AlignLeft, 'English')
        if self.choosed != 1:
            self.painter.setPen(self.gui_settings.Colors['object_color'])
        else:
            self.painter.setPen(self.gui_settings.Colors['selected_color'])

        self.painter.drawText(25, 50, 100, 50, Qt.AlignLeft, 'Русский')

        self.update()
        self.painter.end()


    def keyPressEvent(self, event):
        key = event.key()

        if key == Qt.Key_W or key == Qt.Key_Up:
            self.choosed -= 1
            self.choosed = self.choosed % 2
        elif key == Qt.Key_S or key == Qt.Key_Down:
            self.choosed += 1
            self.choosed = self.choosed % 2
        elif key == Qt.Key_E:
            self.w.change_language(self.choosed)
        else:
            super().keyPressEvent(event)

import os
import sys
import math
from PyQt5.QtGui import QImage


def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


class Printer:

    def __init__(self, x=300, y=275):
        self.x = x
        self.y = y
        self.item = None
        self.img = QImage(resource_path('Images/printer.png'))
        self.active = False

    def draw(self, w, painter):
        self.x = w.width() - 100
        painter.drawImage(self.x, self.y, self.img)
        if self.active:
            if self.item:
                w.player.idea = "..."
                if self.item.x > 50:
                    self.item.x -= 10.0 + w.wind.vx
                    self.item.y += w.wind.vy
                    self.item.draw(w, painter)
                else:
                    w.tree.copy_item(w.player)
                    self.item = None
                    self.active = False

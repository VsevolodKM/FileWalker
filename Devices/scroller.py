import os
import sys
import math
from PyQt5.QtGui import QImage, QPen
from PyQt5.QtCore import Qt


def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


class Scroller:

    def __init__(self, x=0, y=25, rotation=0, active=False):
        self.x = x
        self.y = y
        self.rope_y = y + 25
        self.rope_x = 35
        self.rotation = rotation
        self.img0 = QImage(resource_path('Images/scroller_off.png'))
        self.img1 = QImage(resource_path('Images/scroller1.png'))
        self.img2 = QImage(resource_path('Images/scroller2.png'))
        self.img3 = QImage(resource_path('Images/scroller3.png'))
        self.active = active

    def tangent_point(self, x, y, x_c, y_c, r_c):
        dx = x - x_c
        dy = y - y_c
        L = math.sqrt(dx * dx + dy * dy)
        itg = r_c / L
        if 1 - itg * itg >= 0:
            jtg = math.sqrt(1 - itg * itg)
        else:
            return 35, 45
        x_tg = -itg * dx * itg + itg * dy * jtg
        y_tg = -itg * dx * jtg - itg * dy * itg
        return x_tg + x_c, y_tg + y_c

    def draw(self, w, painter):
        self.rotation = self.rotation % 3
        if not self.active:
            painter.drawImage(int(self.x), int(self.y), self.img0)
        elif self.rotation == 0:
            painter.drawImage(int(self.x), int(self.y), self.img1)
        elif self.rotation == 1:
            painter.drawImage(int(self.x), int(self.y), self.img2)
        else:
            painter.drawImage(int(self.x), int(self.y), self.img3)
        if self.active:
            painter.setPen(QPen(w.gui_settings.Colors['selected_color'], 3, Qt.SolidLine))
        else:
            painter.setPen(QPen(w.gui_settings.Colors['object_color'], 3, Qt.SolidLine))
        painter.drawLine(5, 45, 5, w.height())
        begin_x = 35
        begin_y = 45
        if self.active:
            self.rope_y = w.player.y + 40
            self.rope_x = w.player.x + 10
            begin_x, begin_y = self.tangent_point(self.rope_x, self.rope_y, 20, 45, 15)
            painter.drawLine(int(begin_x), int(begin_y), int(self.rope_x), int(self.rope_y))
        else:
            if self.rope_y != w.height() / 2:
                if self.rope_y - w.height() / 2 > 5.0:
                    self.rope_y -= 5.0
                elif w.height() / 2 - self.rope_y > 5.0:
                    self.rope_y += 5.0
            if self.rope_x != 35:
                if self.rope_x > 35:
                    self.rope_x -= 5.0
                else:
                    self.rope_x += 5.0
            begin_x, begin_y = self.tangent_point(self.rope_x, self.rope_y, 20, 45, 15)
            painter.drawLine(int(begin_x), int(begin_y), int(self.rope_x), int(self.rope_y))

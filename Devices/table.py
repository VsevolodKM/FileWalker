import os
import sys
import math
from PyQt5.QtGui import QImage


def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


class Table:

    def __init__(self, x=300, y=25):
        self.x = x
        self.y = y
        self.img = QImage(resource_path('Images/table.png'))

    def draw(self, w, painter):
        self.x = w.width() - 100
        painter.drawImage(self.x, self.y, self.img)

import os
import sys
import math
from PyQt5.QtGui import QImage, QPen
from PyQt5.QtCore import Qt


def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


class Bin:

    def __init__(self, x=300, y=400):
        self.x = x
        self.y = y
        self.vx = 2.0
        self.vy = -5.0
        self.g = 0.1
        self.item_x = 0
        self.item_y = 0
        self.item_img = None
        self.selected = False
        self.active = False
        self.min_dist = 350
        self.img = QImage(resource_path('Images/bin.png'))

    def draw(self, w, painter):
        self.x = w.width() - 100
        self.y = max(w.height() - 115, 400)
        self.check_player(w.player)
        painter.drawImage(int(self.x), int(self.y), self.img)
        if self.selected:
            painter.setPen(QPen(w.gui_settings.Colors['selected_color'], 3, Qt.SolidLine))
        else:
            painter.setPen(QPen(w.gui_settings.Colors['object_color'], 3, Qt.SolidLine))
        painter.drawLine(int(self.x - self.min_dist + 50), int(self.y + 90), int(self.x), int(self.y + 90))

        if self.active:
            w.player.idea = '...'
            if not (abs(self.item_x + 10 - self.x - 50) < 32 and abs(self.item_y + 10 - self.y - 50) < 45 or self.item_y > w.height() + 100):
                self.item_x += self.vx + w.wind.vx
                self.item_y += self.vy + w.wind.vy
                self.vy += self.g
                painter.drawImage(int(self.item_x), int(self.item_y), self.item_img)
            else:
                self.active = False
                if abs(self.item_x + 10 - self.x - 50) < 25 and abs(self.item_y + 10 - self.y - 10) < 10:
                    w.tree.remove_item(w.player)
                self.item_x = 0
                self.item_y = 0
                self.vy = -5.0

    def check_player(self, player):
        if ((self.x + 50 - player.x - 25) < self.min_dist
                and player.x < self.x
                and abs(player.y + 25 - self.y - 50) < 75
                and not player.select):
            self.selected = True
        else:
            self.selected = False

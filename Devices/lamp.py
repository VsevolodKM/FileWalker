import os
import sys
import math
from PyQt5.QtGui import QImage


def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


class Lamp:

    def __init__(self, x=300, y=125, turn=False):
        self.x = x
        self.y = y
        self.turn = turn
        self.img_off = QImage(resource_path('Images/lamp_off.png'))
        self.img_on = QImage(resource_path('Images/lamp_on.png'))

    def draw(self, w, painter):
        self.x = w.width() - 100
        if self.turn:
            painter.drawImage(self.x, self.y, self.img_on)
        else:
            painter.drawImage(self.x, self.y, self.img_off)

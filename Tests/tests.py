import os
import sys
here = os.path.dirname(__file__)
from PyQt5.QtCore import Qt

sys.path.append(os.path.join(here, '..'))

import pytest

from FileSystem.item import Item
from FileSystem.tree import Tree
from Player.player import Player
from Devices.scroller import Scroller
from Devices.bin import Bin

class MockWindow:
    def __init__(self):
        self.language = 1


def test_default_object_is_default():
    obj = Item()
    assert obj.name in 'DefaultItem' and obj.type == True


def test_is_hidden():
    tree = Tree()
    assert not tree.is_hidden('qwerty') and tree.is_hidden('.qwerty')

def test_player_get_xp():
    w = MockWindow()
    player = Player(w)
    player.get_xp(5)
    assert player.xp == 5

def test_player_new_level():
    w = MockWindow()
    player = Player(w)
    player.get_xp(10)
    player.get_xp(15)
    assert player.level == 2 and player.xp == 5 and player.xp_in_level == 40

def test_player_say():
    w = MockWindow()
    player = Player(w)
    player.say('NoFile')
    assert player.idea in Player.NoFile[player.l]

def test_bin_near():
    w = MockWindow()
    player = Player(w)
    player.say('NoFile')
    bin = Bin(300, 400)
    player.x = 270
    player.y = 400
    bin.check_player(player)
    assert bin.selected == True

def test_bin_far():
    w = MockWindow()
    player = Player(w)
    player.say('NoFile')
    bin = Bin(300, 400)
    player.x = 270
    player.y = 300
    bin.check_player(player)
    assert bin.selected == False

def test_tangent_point_zeros():
    scroller = Scroller()
    assert scroller.tangent_point(20, 20, 0, 0, 0) == (0, 0)

def test_tangent_point():
    scroller = Scroller()
    assert scroller.tangent_point(20, 20, 15, 14, 6) == (15, 8)

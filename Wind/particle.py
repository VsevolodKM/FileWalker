import random

from PyQt5.QtGui import QBrush, QPen
from PyQt5.QtCore import Qt


class Particle:
    def __init__(self, x, y, size, vx, vy):
        self.x = x
        self.y = y
        self.size = size
        self.vx = vx
        self.vy = vy

    def draw(self, w, painter):
        self.x += self.vx / 15 * self.size
        self.y += self.vy / 15 * self.size

        if self.x <= -100:
            self.x = w.width() + 50
            self.y = random.randint(-50, w.height() + 50)
            self.size = random.randint(10, 60)

        if self.x >= w.width() + 100:
            self.x = -50
            self.y = random.randint(-50, w.height() + 50)
            self.size = random.randint(10, 60)

        if self.y <= -100:
            self.y = w.height() + 50
            self.x = random.randint(-50, w.width() + 50)
            self.size = random.randint(10, 60)

        if self.y >= w.height() + 100:
            self.y = -50
            self.x = random.randint(-50, w.width() + 50)
            self.size = random.randint(10, 60)

        painter.setOpacity(0.2)
        painter.setBrush(QBrush(w.gui_settings.Colors['object_color']))
        painter.setPen(QPen(Qt.transparent))
        painter.drawRect(int(self.x), int(self.y), int(self.size), int(self.size))
        #painter.drawEllipse(self.x, self.y, self.size, self.size)

        painter.setOpacity(1.0)

import random

from .particle import Particle

class Wind:
    def __init__(self, w):
        self.w = w
        self.particles_number = 70
        self.max_number = 150
        self.vx = -1.0
        self.vy = -1.0
        self.particle_system = [Particle(random.randint(-50, w.width() + 50),
                                         random.randint(-50, w.height() + 50),
                                         random.randint(10, 60),
                                         self.vx,
                                         self.vy) for i in range(self.max_number)]

    def draw(self, w, painter):
        if random.randint(0, 2000) == 0:
            self.change_wind()
        for i in range(self.particles_number):
            self.particle_system[i].vx = self.vx
            self.particle_system[i].vy = self.vy
            self.particle_system[i].draw(w, painter)

    def change_wind(self):
        new_x = random.randint(-2, 2) / 2
        new_y = random.randint(-2, 2) / 2
        if abs(self.vx + new_x != 0) or (abs(self.vy + new_y) != 0):
            if abs(self.vx + new_x) <= 1.0:
                self.vx += new_x
            if abs(self.vy + new_y) <= 1.0:
                self.vy += new_y

import os
import subprocess
import threading
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtCore import Qt

from .line import Line


class TerminalEditor(QLineEdit):

    def __init__(self, w):
        super().__init__(w)
        self.w = w
        self.x = 3
        #self.subpr = subprocess.Popen([' '], stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
        self.y = w.height() - 33
        self.setStyleSheet('background-color: ' + self.w.gui_settings.Colors['terminal_color'].name() +
                           '; color: ' + self.w.gui_settings.Colors['selected_color'].name() +
                           '; border: 1px solid ' + self.w.gui_settings.Colors['object_color'].name())
        self.thr = threading.Thread(target=self.recv_line)
        self.thr.daemon = True
        self.thr.start()

    def recv_line(self):
        while True:
            try:
                texts = self.subpr.stdout
            except:
                texts = ''
            for text in texts:
                if text.decode() != '':
                    self.w.lines.append(Line(text.decode(), 1))
            try:
                errors = self.subpr.stderr
                for error in errors:
                    if error.decode() != '':
                        self.w.lines.append(Line(error.decode(), 2))
            except:
                pass

    def keyPressEvent(self, event):
        key = event.key()

        if key == Qt.Key_Return or key == Qt.Key_Enter:
            self.w.lines.append(Line(self.text(), 0))

            if self.text().startswith('cd'):
                try:
                    os.chdir(self.text()[3:])
                except OSError:
                    self.w.lines.append(Line('Error', 2))
            else:
                try:
                    self.subpr = subprocess.Popen(self.text().split(), stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
                except:
                    self.w.lines.append(Line('Error', 2))
            self.w.w.tree.update()
            self.setText('')
        else:
            super().keyPressEvent(event)

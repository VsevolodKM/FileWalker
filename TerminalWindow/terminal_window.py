from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtGui import QPainter, QBrush, QPen
from PyQt5.QtCore import Qt, QBasicTimer

from GUI_Settings.gui_settings import GUISetting
from Wind.wind import Wind
from .terminal_editor import TerminalEditor


class TerminalWindow(QMainWindow):

    def __init__(self, w):
        super().__init__(w)
        self.w = w
        self.setWindowTitle('File Walker')
        self.gui_settings = GUISetting()
        self.painter = QPainter(self)
        self.wind = Wind(self)
        self.lines = []
        self.line_edit = TerminalEditor(self)
        self.timer = QBasicTimer()
        self.timer.start(20, self)
        self.initUI()

    def initUI(self):
        self.setAttribute(Qt.WA_NoSystemBackground, True)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setGeometry(10, 10, 600, 400)



    def paintEvent(self, event):
        super().paintEvent(event)
        self.painter.begin(self)
        self.painter.setOpacity(0.938)

        self.painter.setBrush(QBrush(self.gui_settings.Colors['terminal_color']))
        self.painter.setPen(QPen(Qt.transparent))
        self.painter.drawRect(self.rect())

        self.wind.draw(self, self.painter)
        self.painter.setOpacity(1.0)
        self.print_lines(self.painter)

        self.painter.setPen(self.gui_settings.Colors['object_color'])
        self.painter.setFont(self.w.gui_settings.Fonts['small_font'])
        self.painter.drawText(5, self.height() - 64, 40, 18, Qt.AlignLeft, ' ___')
        self.painter.drawText(4, self.height() - 48, 40, 18, Qt.AlignLeft, '|o o|')
        self.painter.drawText(4, self.height() - 44, 40, 18, Qt.AlignLeft, ' ___')
        self.painter.drawText(3, self.height() - 27, 40, 18, Qt.AlignLeft, '-[$]-')
        self.painter.drawText(5, self.height() - 9, 40, 18, Qt.AlignLeft, "  ' ' ")

        #self.update()

        self.painter.end()

    def print_lines(self, painter):
        for i in range(len(self.lines)):
            if self.lines[i].type == 0:
                painter.setPen(self.gui_settings.Colors['selected_color'])
            elif self.lines[i].type == 1:
                painter.setPen(self.gui_settings.Colors['object_color'])
            else:
                painter.setPen(self.gui_settings.Colors['error_color'])
            painter.setFont(self.w.gui_settings.Fonts['basic_font'])
            painter.drawText(3, self.height() - 50 - (len(self.lines) - i) * 20, self.width() - 3, 20, Qt.AlignLeft, self.lines[i].text)

    def timerEvent(self, event):
        if event.timerId() == self.timer.timerId():
            self.line_edit.x = 48
            self.line_edit.y = self.height() - 43
            self.line_edit.setGeometry(self.line_edit.x, self.line_edit.y, self.width() - 53, 40)
            self.update()
        else:
            super().timerEvent(event)

#!/Users/vsevolod/PycharmProjects/FileWalker/venv/bin/python3
# -*- coding: utf-8 -*-

import sys
import os

from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QIcon

from Windows.window import Window


def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


if __name__ == '__main__':

    app = QApplication(sys.argv)

    app.setWindowIcon(QIcon(resource_path('Images/icon2.png')))
    app.setApplicationDisplayName('File Walker')
    app.setApplicationName('File Walker')
    app.setApplicationVersion('1.1.0')

    w = Window()

    sys.exit(app.exec_())
